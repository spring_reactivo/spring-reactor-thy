package com.kenyo.service;

import com.kenyo.document.Plato;

import reactor.core.publisher.Flux;

public interface IPlatoService extends ICRUD<Plato, String>{

	Flux<Plato> buscarPorNombre(String nombre);

	
}
