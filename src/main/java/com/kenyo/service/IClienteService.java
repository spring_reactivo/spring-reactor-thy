package com.kenyo.service;

import org.springframework.data.domain.Pageable;

import com.kenyo.document.Cliente;
import com.kenyo.pagination.PageSupport;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IClienteService extends ICRUD<Cliente, String> {

	Flux<Cliente> listarDemorado();
	Flux<Cliente> listarSobrecargado();
	Mono<PageSupport<Cliente>> listarPagina(Pageable page);
}
