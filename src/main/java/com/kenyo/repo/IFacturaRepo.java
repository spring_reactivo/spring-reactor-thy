package com.kenyo.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.kenyo.document.Factura;

public interface IFacturaRepo extends ReactiveMongoRepository<Factura, String>{

}
