package com.kenyo.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.kenyo.document.Cliente;

public interface IClienteRepo extends ReactiveMongoRepository<Cliente, String> {

}
