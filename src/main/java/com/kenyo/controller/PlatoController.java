package com.kenyo.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.kenyo.document.Plato;
import com.kenyo.service.IPlatoService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/platos")
@SessionAttributes("plato")
public class PlatoController {

	private static final Logger log = LoggerFactory.getLogger(PlatoController.class);
	
	@Autowired
	private IPlatoService service;
	
	@GetMapping("/listar")
	public Mono<String> listar(Model model) {
		
		Flux<Plato> platos = service.listar();
		
		platos.doOnNext(p -> log.info(p.getNombre())).subscribe();
		
		model.addAttribute("platos", platos);
		model.addAttribute("titulo", "Listado de Platos");
		return Mono.just("platos/listar");
	}
	
	@GetMapping("/form")
	public Mono<String> crear(Model model){
		model.addAttribute("plato", new Plato());
		model.addAttribute("titulo", "Formulario de plato");
		model.addAttribute("boton", "Crear");
		return Mono.just("platos/form");
	}
	
	@GetMapping("/form/{id}")
	public Mono<String> editar(@PathVariable("id") String id, Model model){
		Mono<Plato> plato = service.listarPorId(id)
						.doOnNext(p -> log.info("Plato: " + p.getNombre()))
						.defaultIfEmpty(new Plato());
		model.addAttribute("plato", plato);
		model.addAttribute("titulo", "Formulario de plato");
		model.addAttribute("boton", "Modificar");
		return Mono.just("platos/form");
	}
	
	@PostMapping("/operar")
	public Mono<String> operar(Plato plato, Model model, SessionStatus status) {
		return service.registrar(plato)
				.doOnNext(p -> log.info("Plato guardado: " + p.getNombre() + " Id: " + p.getId()))
				.thenReturn("redirect:/platos/listar?success=plato+guardado");
				
	}
	
	@GetMapping("/eliminar/{id}")
	public Mono<String> eliminar(@PathVariable("id") String id){
		return service.listarPorId(id)
					.defaultIfEmpty(new Plato())
					.flatMap(p-> {
						if(p.getId() == null) {
							return Mono.error(new InterruptedException("No existe ID"));
						}
						return Mono.just(p);
					}).flatMap(p -> {
						return service.eliminar(p.getId());
					})
					.then(Mono.just("redirect:/platos/listar"))
							.onErrorResume(ex -> Mono.just("redirect:/platos/listar?error=Error%20Interno"));

	}
	
	@GetMapping(value = "/cargarplatos/{term}", produces = { "application/json" })
	public @ResponseBody Flux<Plato> buscarPorNombre(@PathVariable String term) {
		Flux<Plato> platos = service.buscarPorNombre(term);		
		platos.doOnNext(p -> {
			log.info("INFO" + p.getNombre());
		});
		return platos;
	}
}

